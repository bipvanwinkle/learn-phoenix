defmodule Astropuppy.Router do
  use Astropuppy.Web, :router
  use Terraform, terraformer: Astropuppy.Terraformers.Astrocat

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Astropuppy do
    pipe_through :api

    get "/users", UserController, :index

    get "/helpers", UserController, :helpers
  end
end
