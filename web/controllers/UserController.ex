defmodule Astropuppy.UserController do
  use Astropuppy.Web, :controller
  import Phoenix.Controller
  alias HTTPotion, as: Potion
  alias Poison.Parser

  def index(conn, params) do
    extra = case params do
      %{"limit" => limit, "offset" => offset} ->
        "?limit=#{limit}&#{offset}"
      %{"limit" => limit} ->
        "?limit=#{limit}"
      _ -> "?limit=48"
    end

    %{body: body} = "https://api.ancestorcloud.com/users" <> extra
      |> Potion.get
    %{"entities" => users} = Parser.parse!(body)
    json conn, users
  end

  def helpers(conn, params) do
    {:ok, pid} = Postgrex.start_link(
      hostname: "localhost",
      username: "bipvanwinkle",
      database: "bipvanwinkle"
    )
    limit = case params do
      %{"limit" => limit} -> limit
      _ -> 48
    end
    query = "
      SELECT user_id,first_name,last_name,email,bio,created_at,avatar_thumb
      FROM users
      WHERE is_helper = true
      LIMIT #{limit}
    "

    response = case Postgrex.query(pid, query, []) do
      {:ok, result} -> %{
        entities: result.rows
          |> Enum.map(fn(row) -> List.zip([result.columns, row]) end)
          |> Enum.map(fn(row) -> Enum.into row, %{} end),
        entityCount: length result.rows
      }
      {:error, error} -> %{error: error}
    end

    json conn, response
  end

end
