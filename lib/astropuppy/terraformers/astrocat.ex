defmodule Astropuppy.Terraformers.Astrocat do
  use Plug.Router
  alias HTTPotion, as: Potion

  plug :match
  plug :dispatch

  # TODO
  # 1) Forward more than just the path (headers, etc...)
  # 2) Handle more than just the GETS

  get _ do
    %{request_path: path, params: params, req_headers: headers} = conn
    paramString = URI.encode_query params
    url = "https://api.ancestorcloud.com"
      <> path
      <> "?"
      <> paramString
    response = Potion.get url, [
      headers: headers,
      timeout: 10_000
    ]

    send_response({:ok, conn, response})
  end

  def send_response(apiResponse) do
    {
      :ok,
      conn,
      %{status_code: code, body: body, headers: headers}
    } = apiResponse

    newHeaders = headers.hdrs
      |> Enum.map(fn {a, b} -> {Atom.to_string(a), b} end)
      |> Enum.filter(fn {a, _} ->
        case a do
         "transfer-encoding" -> false
         _ -> true
        end
      end)
    conn = %{conn | resp_headers: newHeaders}
    send_resp(conn, code, body)
  end
end
